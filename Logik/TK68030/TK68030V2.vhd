----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:05:01 09/18/2017 
-- Design Name: 
-- Module Name:    TK68030V2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity TK68030V2 is
    Port ( 
		   AS_030 : inout  STD_LOGIC;
           AS_000 : inout  STD_LOGIC;
           RW_000 : inout  STD_LOGIC;
           DS_030 : inout  STD_LOGIC;
           UDS_000 : inout  STD_LOGIC;
           LDS_000 : inout  STD_LOGIC;
           SIZE : inout  STD_LOGIC_VECTOR (1 downto 0);
           A : inout  STD_LOGIC_VECTOR (31 downto 0);
           BG_030 : in  STD_LOGIC;
           BG_000 : out  STD_LOGIC;
           BGACK_000 : in  STD_LOGIC;
           BGACK_030 : out  STD_LOGIC;
		   BR_030 : out  STD_LOGIC;
           BR_000 : in  STD_LOGIC;
           CLK_030 : out  STD_LOGIC;
		   CLK_FPU : out  STD_LOGIC;
           CLK_000 : in  STD_LOGIC;
           CLK_PLL : in  STD_LOGIC;
           IPL_030 : inout  STD_LOGIC_VECTOR (2 downto 0);
           IPL_000 : in  STD_LOGIC_VECTOR (2 downto 0);
           DSACK : out  STD_LOGIC_VECTOR (1 downto 0);
		   STERM : out  STD_LOGIC;
           DTACK : inout  STD_LOGIC;
           AVEC : out  STD_LOGIC;
           E : out  STD_LOGIC;
           VPA : in  STD_LOGIC;
           VMA : out  STD_LOGIC;
           RESET : in  STD_LOGIC;
           RW_030 : inout  STD_LOGIC;
           AMIGA_BUS_DATA_DIR : out  STD_LOGIC;
           AMIGA_BUS_ENABLE_LOW : out  STD_LOGIC;
           AMIGA_BUS_ENABLE_HIGH : out  STD_LOGIC;
           D : inout  STD_LOGIC_VECTOR (31 downto 28);
           ARAM : out  STD_LOGIC_VECTOR (12 downto 0);
           DQ : out  STD_LOGIC_VECTOR (3 downto 0);
           RAS : out  STD_LOGIC;
           CAS : out  STD_LOGIC;
           MEM_WE : out  STD_LOGIC;
           CLK_RAM : out  STD_LOGIC;
           CLK_EN : out  STD_LOGIC;
           BA : out  STD_LOGIC_VECTOR (1 downto 0);
           OE_030_RAM : out  STD_LOGIC;
           LE_RAM_030 : out  STD_LOGIC;
           OE_RAM_030 : out  STD_LOGIC;
           IDE_BUF_DIR : out  STD_LOGIC;
           IDE_R : out  STD_LOGIC;
           IDE_W : out  STD_LOGIC;
           IDE_WAIT : in  STD_LOGIC;
           ROM_EN : out  STD_LOGIC;
           HALF_SPEED : in  STD_LOGIC;
           OPTION : IN  STD_LOGIC;
           CIIN : out  STD_LOGIC;
		   CBACK: out  STD_LOGIC;
		   CBREQ: in  STD_LOGIC;
			BERR : inout  STD_LOGIC;
			RMC: in  STD_LOGIC;
			FPU_SENSE: in  STD_LOGIC;
			FPU_CS: out  STD_LOGIC;
			FC : in  STD_LOGIC_VECTOR (2 downto 0)
			  
			  );
end TK68030V2;

architecture Behavioral of TK68030V2 is

Function to_std_logic(X: in Boolean) return Std_Logic is
   variable ret : std_logic;
	begin
		if x then 
			ret := '1';  
		else 
			ret := '0'; 
		end if;
   return ret;
end to_std_logic;
	
function MAX(LEFT, RIGHT: INTEGER) return INTEGER is
begin
	if LEFT > RIGHT then 
		return LEFT;
	else 
		return RIGHT;
	end if;
end;
constant MEMORY_128MB: STD_LOGIC:= '0';

constant AUTO_PRECHARGE : STD_LOGIC := '0';
constant PRECHARGE_ALL : STD_LOGIC := '1';
constant NQ_TIMEOUT : integer := 6; --cl2
constant FW_VERSION : STD_LOGIC_VECTOR(3 downto 0) := x"5";
	--wait this number of cycles for a refresh
	--should be 60ns minus one cycle, because the refresh command counts too 150mhz= 6,66ns *9 =60ns
	--puls one cycle for safety :(

constant RQ_TIMEOUT : integer := 50;
	--8192 refreshes in 64ms = one refresh after 7800 ns = 55 7MHz-Clocks



	TYPE sdram_state_machine_type IS (
				powerup, 					
				init_precharge,			
				init_precharge_commit,  
				init_opcode,				
				init_opcode_wait,	
				init_opcode_wait2,				
				start_state,				
				refresh_start,				
				refresh_wait,
				start_ras,				
				commit_ras,			
				start_cas,			
				commit_cas,			
				data_wait,			
				data_wait2,			
				precharge,			
				precharge_wait			
				);

constant AS_SAMPLE : integer := 4;
constant CLK_000_DELAY : integer := AS_SAMPLE;

TYPE SM_E IS (
				E1,
				E2,
				E3,
				E4,
				E5,
				E6,
				E7,
				E8,
				E9,
				E10
				);

TYPE SM_68000 IS (
				IDLE_P,
				IDLE_N,
				AS_SET_P,
				AS_SET_N,
				SAMPLE_DTACK_P,
				DATA_FETCH_N,
				DATA_FETCH_P,
				END_CYCLE_N,
				DMA_IDLE_N,
				DMA_AS_SET_N,
				DMA_END_CYCLE_N,
				DMA_FINISHED_P1,
				DMA_FINISHED_P2
				);										
				
signal	cpu_est : SM_E;
signal	SM_AMIGA : SM_68000;
signal 	CQ :  sdram_state_machine_type;

attribute fsm_encoding : string;
attribute fsm_encoding of cpu_est : signal IS "gray";
attribute fsm_encoding of SM_AMIGA : signal is "gray";
attribute fsm_encoding of CQ  : signal is "gray";
--Savestate is said to be buggy on CPLDs
--attribute fsm_safe_state : string;
--attribute fsm_safe_state of cpu_est : signal is "E1";
--attribute fsm_safe_state of SM_AMIGA : signal is "IDLE_P";
--attribute fsm_safe_state of CQ  : signal is "powerup";

signal	ESig:STD_LOGIC :='1';
signal	AS_000_INT:STD_LOGIC :='1';
signal	RW_000_INT:STD_LOGIC :='1';
signal	AMIGA_BUS_ENABLE_DMA_HIGH:STD_LOGIC :='1';
signal	AMIGA_BUS_ENABLE_DMA_LOW:STD_LOGIC :='1';
signal	AS_030_D0:STD_LOGIC :='1';
signal	AS_030_D1:STD_LOGIC :='1';
signal	AS_030_000_SYNC:STD_LOGIC :='1';
signal	BGACK_030_INT:STD_LOGIC :='1';
signal	BGACK_000_SAMPLED:STD_LOGIC	:= '1';
signal	AS_000_DMA:STD_LOGIC :='1';
signal	DS_000_DMA:STD_LOGIC :='1';
signal	RW_000_DMA:STD_LOGIC :='1';
signal	SIZE_DMA: STD_LOGIC_VECTOR ( 1 downto 0 ):="11";
signal	IPL_D0: STD_LOGIC_VECTOR ( 2 downto 0 ):="111";
signal	IPL_D1: STD_LOGIC_VECTOR ( 2 downto 0 ):="111";
signal	Z2_ADR1: STD_LOGIC_VECTOR ( 2 downto 0 ):="111";
signal	Z2_ADR2: STD_LOGIC_VECTOR ( 2 downto 0 ):="111";
signal	A0_DMA: STD_LOGIC :='0';
signal	VMA_INT: STD_LOGIC :='1';
signal	UDS_000_INT: STD_LOGIC :='1';
signal	LDS_000_INT: STD_LOGIC :='1';
signal	DS_000_ENABLE: STD_LOGIC :='0';
signal	CLK_000_D: STD_LOGIC_VECTOR ( CLK_000_DELAY downto 0 ) := (others =>'1');
signal	CLK_000_PE: STD_LOGIC :='1';
signal	CLK_000_NE: STD_LOGIC :='1';
signal	CLK_FPU_S: STD_LOGIC;
signal	CLK_CPU: STD_LOGIC;
signal	CLK_CPU_D: STD_LOGIC;
signal	CLK_HALF: STD_LOGIC;
signal	DTACK_D0: STD_LOGIC :='1';
signal	CLK_GEN: STD_LOGIC_VECTOR ( 1 downto 0 ):="00";
signal	RESET_D0: STD_LOGIC :='0';
signal	RESET_D1: STD_LOGIC :='0';
signal	AMIGA_DS: STD_LOGIC :='1';
signal	TK_CYCLE: STD_LOGIC :='0';
signal  IDE_SPACE:STD_LOGIC :='0';
signal  MEM_SPACE:STD_LOGIC :='0';

signal	AUTO_CONFIG:STD_LOGIC :='0';
signal	AUTO_CONFIG_DONE:STD_LOGIC_VECTOR(1 downto 0) :=(others =>'0');
signal	AUTO_CONFIG_PAUSE:STD_LOGIC :='0';
signal	AUTO_CONFIG_DONE_CYCLE:STD_LOGIC_VECTOR(1 downto 0) := (others => '0');
signal	SHUT_UP_IDE:STD_LOGIC :='1';
signal  SHUT_UP_MEM:STD_LOGIC :='1';
signal	IDE_BASEADR:STD_LOGIC_VECTOR(7 downto 0) := (others => '0');
signal	Dout:STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
signal	Dout1:STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
signal	Dout2:STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
signal	IDE_DSACK_D:STD_LOGIC_VECTOR(3 downto 0) := (others => '0');
signal	IDE_DELAY:STD_LOGIC_VECTOR(3 downto 0):= (others => '0');
signal	IDE_READY:STD_LOGIC :='0';
signal	Dout_ENABLE:STD_LOGIC :='0';
signal	DSACK_16BIT_AMIGA:STD_LOGIC :='0';
signal	DSACK_16BIT_IDE:STD_LOGIC :='0';
signal	DSACK_16BIT_AUTOCONFIG:STD_LOGIC :='0';
signal	IDE_ENABLE:STD_LOGIC:='0';
signal	ROM_OE_S:STD_LOGIC:='1';
signal	IDE_R_S:STD_LOGIC:='1';
signal	IDE_W_S:STD_LOGIC:='1';
signal	IDE_WAIT_D:STD_LOGIC:='1';
signal 	E_DTACK :  STD_LOGIC:='1';
signal 	FPU_CYCLE :  STD_LOGIC:='1';
signal 	AVEC_CYCLE :  STD_LOGIC:='1';
signal	TRANSFER_IN_PROGRES:STD_LOGIC:= '0';
signal	REFRESH: std_logic:= '0';
signal	BURST: std_logic:= '0';
signal	NQ :  STD_LOGIC_VECTOR (2 downto 0):=(others => '0');
signal	RQ :  STD_LOGIC_VECTOR (5 downto 0):=(others => '0');

constant ARAM_OPTCODE: STD_LOGIC_VECTOR (12 downto 0) := "0001000100010"; --cl2
signal	ENACLK_PRE : STD_LOGIC :='1';
signal	STERM_S : STD_LOGIC :='1';
signal	STERM_S2 : STD_LOGIC :='1';
signal	CBACK_S : STD_LOGIC :='1';
signal	RESET_S : STD_LOGIC :='0';
signal	burst_counter : STD_LOGIC_VECTOR(1 downto 0) :="11";
signal	RAM_BANK_ACTIVATE  :  STD_LOGIC :='0';
signal	LE_RAM_CPU_P : STD_LOGIC :='1';
signal	SCAN_CBACK : STD_LOGIC :='0';
signal	Z2_MODE : STD_LOGIC :='0';
signal	FPU_SPEED : STD_LOGIC :='0';

--- additional signals for the maprom feature

signal	ROM_SPACE : STD_LOGIC_VECTOR (1 downto 0) :="00";                                                                    
signal	ROM_OVERLAY_ENABLE : STD_LOGIC_VECTOR(3 downto 0) :="0000"; 
signal	CHIP_ROM_OVERLAY_ENABLE : STD_LOGIC:='0';

begin

	CLK_000_PE <= CLK_000_D(0) AND NOT CLK_000_D(1);
	CLK_000_NE <= NOT CLK_000_D(0) AND CLK_000_D(1);
	--output clock assignment
	CLK_030 <= CLK_CPU;--CLK_GEN(0) when HALF_SPEED ='1' else CLK_GEN(1);--to_std_logic((CLK_GEN(0) = '1' and HALF_SPEED ='1') or ( CLK_GEN(1) ='1' and HALF_SPEED ='0'));
	CLK_FPU <= CLK_FPU_S;--CLK_GEN(0) when OPTION ='1' else CLK_GEN(1);
	
	--internal clock
	--CLK_CPU <= CLK_GEN(0) when HALF_SPEED ='1' else CLK_GEN(1);
	
	--pos edge clock process
	--no ansynchronious reset! the reset is sampled synchroniously
	--this mut be because of the e-clock: The E-Clock has to run CONSTANTLY 
	--or the Amiga will fail to boot from a reset. 
	--However a compilation with no resets on the E-Clock and resets on other signals does not work, either!
	FPU_SPEED <= OPTION;
	gen_clk: process(CLK_PLL)
	begin
		if(rising_edge(CLK_PLL)) then
			CLK_GEN <= CLK_GEN+1;
			case(CLK_GEN( 1 downto 0)) is
				when "00" =>
					if(HALF_SPEED ='1')then
						CLK_CPU <='1';
					else
						CLK_CPU <='0';
					end if;
					if(FPU_SPEED ='1')then
						CLK_FPU_S <='1';
					else
						CLK_FPU_S <='0';
					end if;
				when "01" =>
					if(HALF_SPEED ='1')then
						CLK_CPU <='0';
					else
						CLK_CPU <='1';
					end if;
					if(FPU_SPEED ='1')then
						CLK_FPU_S <='0';
					else
						CLK_FPU_S <='1';
					end if;
				when "10" =>
					if(HALF_SPEED ='1')then
						CLK_CPU <='1';
					else
						CLK_CPU <='1';
					end if;
					if(FPU_SPEED ='1')then
						CLK_FPU_S <='1';
					else
						CLK_FPU_S <='1';
					end if;
				when others => --"11"
					if(HALF_SPEED ='1')then
						CLK_CPU <='0';
					else
						CLK_CPU <='0';
					end if;
					if(FPU_SPEED ='1')then
						CLK_FPU_S <='0';
					else
						CLK_FPU_S <='0';
					end if;
			end case;
			CLK_CPU_D <= CLK_CPU;
			--delayed Clocks and signals for edge detection
			CLK_000_D(0) 	<= CLK_000;
			CLK_000_D(CLK_000_DELAY downto 1) 	<= CLK_000_D((CLK_000_DELAY-1) downto 0);
			
			--edge detection
			RESET_D0 <= RESET;
			RESET_D1 <= RESET_D0;

			
			-- e-clock is changed on the FALLING edge!

			if(CLK_000_NE = '1') then
				case (cpu_est) is
					when E1  =>	cpu_est <= E2 ; 
					when E2  => cpu_est <= E3 ;
					when E3  => cpu_est <= E4;
								if(VPA = '0' and AS_000_INT ='0')then				
									VMA_INT <= '0'; --assert
								end if;
					when E4  => cpu_est <= E5 ;
					when E5  => cpu_est <= E6 ;
					when E6  => cpu_est <= E7 ;
								ESig <='1';
					when E7  => cpu_est <= E8 ;
					when E8  => cpu_est <= E9 ;
								if(VMA_INT = '0') then
									E_DTACK <='0'; --generate a DTACK at the right time
								end if;
					when E9  => cpu_est <= E10;
					when E10 => cpu_est <= E1 ;
								ESig <='0';
				end case;
			end if;
			
			if(AS_000_INT ='1') then
				E_DTACK <='1';
				VMA_INT <= '1'; --deassert
			end if;
			
		end if;
	end process gen_clk;

	TK_CYCLE <='0' WHEN MEM_SPACE='1' or AUTO_CONFIG='1' or IDE_SPACE ='1' else '1';

	AMIGA_SM: process(CLK_PLL,RESET)
	begin
		if(RESET = '0' ) then
			DTACK_D0		<= '1';
			SM_AMIGA		<= IDLE_P;
			AS_000_INT 		<= '1';
			RW_000_INT		<= '1';
			RW_000_DMA		<= '1';
			AS_030_000_SYNC <= '1';
			UDS_000_INT		<= '1';
			LDS_000_INT		<= '1';
			DS_000_ENABLE	<= '0';
			BG_000			<= '1';
			BGACK_030_INT <= '1';
			DSACK_16BIT_AMIGA		<= '0';
			IPL_D0			<= "111";
			IPL_D1			<= "111";
			IPL_030			<= "111";
			AS_000_DMA		<= '1';
			DS_000_DMA		<= '1';
			SIZE_DMA		<= "11";
			A0_DMA			<= '1';
			AMIGA_BUS_ENABLE_DMA_HIGH <= '1';
			AMIGA_BUS_ENABLE_DMA_LOW <= '1';
			AS_030_D0		<= '1';
			AS_030_D1		<= '1';
			AMIGA_DS <='1';
			AVEC_CYCLE <='1';
			BGACK_000_SAMPLED <='1';
		elsif(rising_edge(CLK_PLL)) then

			--the statemachine

			--buffering signals
			AS_030_D0 <= AS_030;
			AS_030_D1 <= AS_030_D0;
			DTACK_D0	<= DTACK;

			--AVEC cycle detection
			if(FC ="111" and A(19 downto 16)= "1111" AND BGACK_000='1') then
				AVEC_CYCLE <= '0';
			else
				AVEC_CYCLE <= '1';
			end if;

			--if(CLK_000_NE='1')then --sample at falling edges!
				BGACK_000_SAMPLED <= BGACK_000;
			--end if;
			--bgack assert is simple: assert as soon as Amiga asserts BGACK
			--deassert happens in the DMA statemachine 
			if(BGACK_000='0' and BGACK_000_SAMPLED ='0') then
				BGACK_030_INT	<= '0';
			end if;
			

			--bus grant only in idle state
			if(BG_030= '1')then
				BG_000	<= '1';
			elsif(	BG_030= '0' AND (SM_AMIGA 	= IDLE_P)
					and AS_030_D0='1' and AS_030 ='1'
					and CLK_000_PE = '1' 
					and RMC ='1'
					) then --bus granted no local access and no AS_030 running!
					BG_000 	<= '0';
			end if;
		
			

		
			--interrupt buffering to avoid ghost interrupts
			IPL_D0<=IPL_000;			
			IPL_D1<=IPL_D0;

			if(	IPL_D0=IPL_D1 
				and CLK_000_PE = '1' 
				--and IPL_D0 = IPL_000
				) then
				IPL_030<=IPL_D0;
			--else
			--	IPL_030<=IPL_030;
			end if;
		
			-- as030-sampling
			if(AS_030 ='1') then -- "async" reset of various signals
				AS_030_000_SYNC <= '1';
				DSACK_16BIT_AMIGA		<= '0';
			elsif(	--CLK_030  		= '1'  AND --68030 has a valid AS on high clocks	
					AVEC_CYCLE = '1' AND --no AVEC
					FPU_CYCLE = '1' AND --no FPU
					AS_030_D0			= '0'  AND --as set
					BGACK_030_INT='1' AND 
					TK_CYCLE = '1' and --not an expansion space cycle
					SM_AMIGA = IDLE_P --last amiga cycle terminated
					) then
					AS_030_000_SYNC <= '0';					
			end if;
			
			--uds/lds precalculation
			if (SM_AMIGA = IDLE_N) then --DS: set udl/lds 	
				if(A(0)='0') then
					UDS_000_INT <= '0';
				else
					UDS_000_INT <= '1';
				end if;
				if((A(0)='1' OR SIZE(0)='0' OR SIZE(1)='1')) then
					LDS_000_INT <= '0';
				else
					LDS_000_INT <= '1';
				end if;
			end if;


			--Amiga statemachine
			case (SM_AMIGA) is
				when IDLE_P 	 => --68000:S0 wait for a falling edge
					RW_000_INT		<= RW_030;		
					if( CLK_000_D(AS_SAMPLE-1)='0' and CLK_000_D(AS_SAMPLE)= '1' and AS_030_000_SYNC = '0' and BGACK_030_INT='1')then -- if this a delayed expansion space detection, do not start an amiga cycle!
						SM_AMIGA<=IDLE_N;  --go to s1
					elsif(BGACK_030_INT = '0')then
						SM_AMIGA <=DMA_IDLE_N; --DMA Start!
					end if;
				when IDLE_N 	 => --68000:S1 place Adress on bus and wait for rising edge, on a rising CLK_000 look for a amiga adressrobe
					if(CLK_000_PE='1')then --go to s2
						SM_AMIGA <= AS_SET_P; --as for amiga set! 
						RW_000_INT <= RW_030;						
						AS_000_INT <= '0';
						if (RW_030='1' ) then --read: set udl/lds 	
							DS_000_ENABLE	<= '1';
						end if;
					end if;
				when AS_SET_P	 => --68000:S2 Amiga cycle starts here: since AS is asserted during transition to this state we simply wait here
					if(CLK_000_NE='1')then --go to s3
						SM_AMIGA<=AS_SET_N; 
					end if;
				when AS_SET_N	 => --68000:S3: nothing happens here; on a transition to s4: assert uds/lds on write 
					
					if(CLK_000_PE='1')then --go to s4
						-- set DS-Enable without respect to RW_030: this simplifies the life for the syntesizer
						DS_000_ENABLE	<= '1';--write: set udl/lds earlier than in the specs. this does not seem to harm anything and is saver, than sampling uds/lds too late 				 
						SM_AMIGA <= SAMPLE_DTACK_P; 
					end if;
				when SAMPLE_DTACK_P=> --68000:S4 wait for dtack or VMA
					if(	CLK_000_NE='1' and --falling edge
						(
						DTACK_D0='0' OR --DTACK end cycle
						BERR='0' OR
						E_DTACK='0'
						)
						)then --go to s5
						SM_AMIGA<=DATA_FETCH_N;
					end if;
				when DATA_FETCH_N=> --68000:S5 nothing happens here just wait for positive clock
					if(CLK_000_PE = '1')then --go to s6
						SM_AMIGA<=DATA_FETCH_P;
					end if;
				when DATA_FETCH_P => --68000:S6: READ: here comes the data on the bus!
					
					if(CLK_000 = '0') then
						DSACK_16BIT_AMIGA <='1'; 
					end if;
				
					if( CLK_000_NE ='1') then 
						SM_AMIGA<=END_CYCLE_N;
						DSACK_16BIT_AMIGA <='1'; 
					end if;
				when END_CYCLE_N =>--68000:S7: Latch/Store data. Wait here for new cycle and go to IDLE on high clock
					AS_000_INT <= '1';
					DS_000_ENABLE	<= '0';
					
					if(CLK_000_PE='1')then --go to s0	
						SM_AMIGA<=IDLE_P;	
						RW_000_INT		<= '1';	
					end if;
				--these states are for DMA-cycles
				when DMA_IDLE_N =>
					AS_000_DMA 	<= '1'; 
					DS_000_DMA	<= '1';
					if(AS_000 = '0' and CLK_000_NE ='1') then 
						AS_000_DMA 	<= not RW_000; 
						DS_000_DMA	<= not RW_000;
						SM_AMIGA<=DMA_AS_SET_N;	
					elsif(BGACK_000_SAMPLED ='1' and AS_000 = '1') then --BGACK_000_SAMPLED is synced to the falling edge
						SM_AMIGA<=DMA_FINISHED_P2;
					end if;
				when DMA_AS_SET_N =>
					if(AMIGA_DS = '0') then --assert internal AS when datastrobe is detected
						AS_000_DMA 	<= '0'; 
						DS_000_DMA	<= '0';
					end if;
					if(CLK_000_NE='1')then	
						AS_000_DMA 	<= '0'; 
						DS_000_DMA	<= '0';
						SM_AMIGA<=DMA_END_CYCLE_N;
					end if;
				when DMA_END_CYCLE_N =>
					if(AS_000 = '1')then
						AS_000_DMA 	<= '1'; 
						DS_000_DMA	<= '1';
					end if;
					if(CLK_000_PE='1' and AS_000 = '1')then	
						SM_AMIGA<=DMA_IDLE_N;
					end if;
				when DMA_FINISHED_P1 =>
					if(CLK_000_PE='1')then	
						SM_AMIGA<=DMA_FINISHED_P2;	
					end if;
				when DMA_FINISHED_P2 =>
					if(CLK_000_PE='1') then --go to s0	
						SM_AMIGA<=IDLE_P;	
						BGACK_030_INT <= '1';	
					end if;
			end case;

			--dma stuff this delays the write strobe a bit to set up the propper width
			if(UDS_000='0' or LDS_000='0') then
				AMIGA_DS <='0';
			else 
				AMIGA_DS <='1';
			end if; 
			
			if(BGACK_030_INT='0')then			
				--set some signals NOT linked to AS_000='0'
				RW_000_DMA	<= RW_000;
				-- now determine the size: if both uds and lds is set its 16 bit else 8 bit!
				


				if(UDS_000='0' and LDS_000='0') then
					SIZE_DMA		<= "10"; --16bit
				else
					SIZE_DMA		<= "01"; --8 bit
				end if;
				--now calculate the offset: 
				--if uds is set low, a0 is so too.
				--if only lds is set a0 high
				--therefore a0 = uds 
				--great! life is simple here!
				A0_DMA <= UDS_000;		

				--A1 is set by the amiga side													
				--here  we determine the upper or lower half of the databus
				if(MEM_SPACE ='1') then
					AMIGA_BUS_ENABLE_DMA_HIGH 	<= A(1);
					AMIGA_BUS_ENABLE_DMA_LOW 	<= not A(1);				
				else -- ide and autoconfig are only 16bit wide and always on the upper bus half!
					AMIGA_BUS_ENABLE_DMA_HIGH 	<= '0';
					AMIGA_BUS_ENABLE_DMA_LOW 	<= '1';
				end if;

			else			
				RW_000_DMA		<= '1';	
				SIZE_DMA		<= "00";
				A0_DMA			<= '0';	
				AMIGA_BUS_ENABLE_DMA_HIGH 	<= '1';
				AMIGA_BUS_ENABLE_DMA_LOW 	<= '1';				
			end if;
		end if;		
	end process ;


	--address decode section: Must be on the negative edge for memory timing. 
	--IDE/AC simply put here too ;) 
	neg_edge_addr:process(CLK_PLL,RESET)
	begin
		if(RESET = '0' ) then
			MEM_SPACE <= '0';
			IDE_SPACE <= '0';
			AUTO_CONFIG <= '0';
			FPU_CYCLE <='1';
		elsif(falling_edge(CLK_PLL))then
			
			--MEM address decode section: allways at $4xxxxxxx to enable addmem under Kick1.3! but disable mem for DMA (it's not accessible!)
			if(A(31 downto 27) = "01000" and not  (A(26 downto 21) = MEMORY_128MB&"11111"))then
                          MEM_SPACE <= '1';
                          ROM_SPACE <= "00";
                        elsif  (A (31 downto 24) = x"00" and A (23 downto 21) = "111" and 
                                ((A (20 downto 19) = "11" and ((ROM_OVERLAY_ENABLE(0) = '1' and RW_030='1') or
                                                               (ROM_OVERLAY_ENABLE(1) = '0' and RW_030='0')))  or
                                 (A(19) = '0' and ((ROM_OVERLAY_ENABLE(2) = '1' and RW_030='1') or
                                                   (ROM_OVERLAY_ENABLE(3) = '0' and RW_030='0')))) 		
                                 and Z2_MODE='0') then
                          MEM_SPACE <= '1';
                          ROM_SPACE <= "01"; --change A(26 downto 21) to '1' at ARAM  access (MapROM is at the end of Z3 RAM).
                        elsif ( (A (31 downto 24) = x"00") and (A (23 downto 19) = x"0"&'0') and CHIP_ROM_OVERLAY_ENABLE = '1'and Z2_MODE='0') then
                          MEM_SPACE <= '1';
                          ROM_SPACE <= "10";
								elsif((A(23 downto 21) = Z2_ADR1 or A(23 downto 21) = Z2_ADR2) and Z2_MODE ='1')then
                          MEM_SPACE <= not SHUT_UP_MEM;
                          ROM_SPACE <= "00";
								else 
                          MEM_SPACE <= '0';
                          ROM_SPACE <= "00";
			end if;
			
			--IDE address decode section 
			if(A(31 downto 16) = (x"00"&IDE_BASEADR) AND SHUT_UP_IDE ='0') then
				IDE_SPACE <= '1';
			else
				IDE_SPACE <= '0';
			end if;
			
			--Autoconfig(tm) address decode section 
			if(A(31 downto 16) =x"00E8" AND AUTO_CONFIG_DONE /="11") then
				AUTO_CONFIG <= '1';
			else
				AUTO_CONFIG <= '0';
			end if;
			
			--FPU cycle detection
			if(FC ="111" and A(19 downto 16)= "0010" AND BGACK_000='1') then
				FPU_CYCLE <= '0';
			else
				FPU_CYCLE <= '1';
			end if;
		end if;
	end process neg_edge_addr;	

	--SDRAM-STATEMACHINE
			
	--all signals, which need to be clocked on the negative edge
	neg_edge_ctrl:process(CLK_PLL)
	begin
		if(falling_edge(CLK_PLL))then
			--latch control for reads
			--LE_RAM_030<= LE_RAM_CPU_P;
			--STERM_S2		<= STERM_S;
		end if;
	end process neg_edge_ctrl;		
	STERM_S2		<= STERM_S;
	LE_RAM_030<= LE_RAM_CPU_P;
	--bussizing decode
	bus_siz_ctrl:process (RAM_BANK_ACTIVATE,CLK_PLL)begin
		if(RAM_BANK_ACTIVATE = '0' )then
			BA 	<= "00";
			DQ<= "1111"; --high during init
		elsif(rising_edge(CLK_PLL)) then
			if( MEM_SPACE ='1' and AS_030='0' and AS_030_D0 = '1') then
			   if ( ROM_SPACE = "10" ) then
                                  BA <=  '1'& A(18);
				else
                                  BA <=  A(19 downto 18);
				end if;
				
				--DQ-mask decoding
				if(RW_030='0')then --mask on non long writes
					--now decode the adresslines A[0..1] and SIZ[0..1] to determine the ram bank to write				
					-- bits 0-7
					if(	SIZE="00" or 
							(A(0) ='1' and A(1)='1') or 
							(A(1)='1' and SIZE(1)='1') or
							(A(0) ='1' and SIZE="11" ))then
						DQ(0)	<= '0';
					else
						DQ(0)	<= '1';
					end if;
							
					-- bits 8-15
					if(	(A(0) ='0' and A(1)='1') or
							(A(0) ='1' and A(1)='0' and SIZE(0)='0') or
							(A(1)='0' and SIZE="11") or 
							(A(1)='0' and SIZE="00"))then
						DQ(1)<= '0';
					else
						DQ(1)<= '1';
					end if;				
						
					--bits 16-23
					if(	(A(0) ='1' and A(1)='0') or
							(A(1)='0' and SIZE(0)='0') or 
							(A(1)='0' and SIZE(1)='1'))then
						DQ(2)	<= '0';
					else
						DQ(2)	<= '1';
					end if;									
						
					--bits 24--31
					if(( 	A(0) ='0' and A(1)='0' ))then
						DQ(3)	<= '0';
					else
						DQ(3)	<= '1';
					end if;
				else
					DQ <= "0000"; --all others: full 32 bit
				end if;
			end if;
		end if;
	end process bus_siz_ctrl;
 
 	--output buffer control	
	OE_030_RAM <= '0' when TRANSFER_IN_PROGRES='1' and RW_030='0' else '1';
	OE_RAM_030 <= '0' when TRANSFER_IN_PROGRES='1' and RW_030='1' else '1';
	
	--MEM address decode section
	--MEM_SPACE <= '1' when A(31 downto 27) = "01000" else '0'; 
	
	--all ram signals, which need to be clocked on the positive edge
	pos_edge_ctrl:process (CLK_PLL,RESET) begin
		if(RESET = '0') then
			RAM_BANK_ACTIVATE <='0';
			CQ	<= powerup;
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ENACLK_PRE <= '1';		 
 			ARAM <= (others => '1');
			RQ<=	(others => '0');
			STERM_S <= '1';
			LE_RAM_CPU_P<= '1';
			TRANSFER_IN_PROGRES <= '0';
			CBACK_S <= '1';
			burst_counter <= "11";
			REFRESH <= '0';
			NQ  <= (others => '0');
			BURST <= '0';
			SCAN_CBACK <= '0';

		elsif rising_edge(CLK_PLL) then		

		
			
			--sterm control
			if(	(CQ=commit_cas and RW_030 = '1') or --first read ready
				(CQ=commit_ras and RW_030 = '0')  or --write can be terminated one 030-clock earlier
				(CQ=commit_ras and HALF_SPEED = '0') --half CPU-Speed = one cycle earlier for read
				)then
				
				STERM_S <= '0' ;
			elsif(AS_030 = '1' 
				)then
				STERM_S <= '1';
			end if;	

			--transfer detection, cacheburst length and cacheburst acknowledge
			if(AS_030='1')then
				TRANSFER_IN_PROGRES <= '0';
				CBACK_S <= '1';
				burst_counter <= "11";
				BURST <= '0';
			elsif (	CQ=commit_ras	)then --start transfer and decode cacheburst
				TRANSFER_IN_PROGRES <= '1';
				--cache burst logic
				if( CBREQ = '0' 
				    and SIZE="00"
					and A(3 downto 2) /= "11"
					and RW_030 ='1'
					)then
					BURST <= '1';
					CBACK_S <='0';
					burst_counter <= A(3 downto 2);
				else
					BURST <= '0';
					CBACK_S <= '1';
					burst_counter <= "11";
				end if;
				
			elsif(CQ=data_wait and CBACK_S ='0')then -- incerement burst counter and stop at the right time
					burst_counter <= burst_counter+1;
			elsif(burst_counter = "11" or CBREQ ='1')then
					CBACK_S <= '1';
			end if;



			--refresh flag
			if(CQ = refresh_start)then
				REFRESH <= '0';
			elsif(RQ = RQ_TIMEOUT) then 
				REFRESH <= '1';
			end if;

			--refresh counter
			if (RQ = RQ_TIMEOUT ) then
				RQ<=	(others => '0');
			elsif(CLK_000_PE= '1') then --count on 7-MHz edges
				RQ <= RQ + 1;
			end if;
			
			--wait counter stuff
			if(CQ = refresh_wait)
			then
				NQ <= NQ + 1;
			else 
				NQ  <= (others => '0');
			end if;				
	
			
			-- default values
			ENACLK_PRE <= '1';		 
			RAS <= '1';
			CAS <= '1';
			MEM_WE <= '1';
			ARAM <= A(17 downto 5);
			LE_RAM_CPU_P<= '1';
			SCAN_CBACK <= '0';
	
			
			-- ram state machine decoder
			case CQ is
			when powerup =>
			 CQ <= init_precharge;					 
			when init_precharge =>
			 ARAM(10) <= '1';			
			 RAS <= '0';
			 MEM_WE <= '0';
			 CQ <= init_precharge_commit;			
			when init_precharge_commit =>
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode;  
			when init_opcode =>
			 RAS <= '0';
			 CAS <= '0';
			 MEM_WE <= '0';
			 ARAM <= ARAM_OPTCODE;			
			 CQ <= init_opcode_wait;
			when init_opcode_wait =>
			 CQ <= init_opcode_wait2; --opcode needs some time to get in the mem brain
			when init_opcode_wait2 =>
			 CQ <= refresh_start;   --1st refresh
			when start_state =>
			 if (REFRESH = '1') then
				 CQ <= refresh_start;
			 elsif (	MEM_SPACE ='1' and TRANSFER_IN_PROGRES='0' and AS_030='0'
						and CLK_CPU='0' and CLK_CPU_D = '1'
						) then
				
				RAS <= '0'; 
				CQ <= commit_ras;
			 end if;			 
			when refresh_start =>
			 RAS <= '0';
			 CAS <= '0';
			 CQ <= refresh_wait;
			when refresh_wait =>
			 if (NQ >= NQ_TIMEOUT) then			--wait 60ns here				 
				 if(RAM_BANK_ACTIVATE ='0')then
					CQ <= refresh_start; --2nd refresh on start up!
				 else
					CQ <= start_state;
				 end if;
				 RAM_BANK_ACTIVATE <='1'; --now its save to switch on the bank decode
			 else
				 CQ <= refresh_wait;
			 end if;
		   when start_ras =>
			 RAS <= '0';
			 CQ<= commit_ras;
		   when commit_ras =>
			 CQ <= start_cas; 
                          when start_cas =>
                            ARAM( 2 downto 0) <= A(4 downto 2);
			    if ( ROM_SPACE = "00") then
                              ARAM(9 downto 3) <=  A(26 downto 20);
                            else
                              ARAM(9 downto 4) <=  "111111";  --ROM is located in last MB
                              if ROM_SPACE = "10" then
                                ARAM(3) <= '1';
                              else 
                                ARAM(3) <= A(20);
                              end if;
                            end if;
			 
			 ARAM(10) <= AUTO_PRECHARGE; 
			 CAS <= '0';
			 MEM_WE <= RW_030;
			 CQ <= commit_cas;
			when commit_cas =>
			 if(burst_counter = "11" and RW_030 = '1' )then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the read (no burst)			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 CQ <= data_wait;	 
			 --LE_RAM_CPU_P<= not RW_030;
			 ENACLK_PRE <= not RW_030; --delay comes one clock later!
			when data_wait => 
			 SCAN_CBACK <= '1';
			 --latch datat on read
			 LE_RAM_CPU_P<= not RW_030;
			 --initiate precharge on a write
			 if((RW_030 ='0' and AUTO_PRECHARGE ='0'))then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the write			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 
			 --see if we reached the end
			 if(burst_counter = "11")then
			 	if(RW_030 = '1')then
					CQ <= start_state; -- allready precharged in commit_cas or burst_counter = "10"	 				
				else
					CQ <= precharge_wait; --wait need two recovery clocks
				end if;
			 else
				CQ <= data_wait2;
			 end if;
			when data_wait2 =>
			 SCAN_CBACK <= '1';
			 --LE_RAM_CPU_P<= not RW_030;
			 --see if we have to stop the burst
			 if(burst_counter = "11" and RW_030 = '1' and BURST ='1')then
				ARAM(10) <= PRECHARGE_ALL; --Precharge termination of the read burst		
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 ENACLK_PRE <= '0';
			 if (CLK_CPU='1' and CLK_CPU_D = '1') or HALF_SPEED ='1' then
				CQ <= data_wait;
			 end if;
			when precharge =>
			 if(AUTO_PRECHARGE ='0')then
				ARAM(10) <= PRECHARGE_ALL;			
				RAS <= '0';
				MEM_WE <= '0';
			 end if;
			 CQ <= start_state; --start is doing the 2nd clock of precharge delay
			when precharge_wait =>
			 CQ <= start_state;  --start is doing the 2nd clock of precharge delay
			end case;
		end if;
   end process pos_edge_ctrl;	

	
	
	data_enable:process(AS_030,CLK_PLL,RW_030)begin
		if(RW_030='0' or AS_030='1')then
			Dout_ENABLE <= '0';
			Dout <= "1111";
		elsif(rising_edge(CLK_PLL))then
			if(AUTO_CONFIG ='1' or (IDE_SPACE = '1' and A(15)='1'))then
				Dout_ENABLE <= '1';
			else
				Dout_ENABLE <= '0';
			end if;

			if(AUTO_CONFIG = '1') then	
				--mem/ide order swapped for kick 1.3!			
				if(AUTO_CONFIG_DONE(0)='1')then
					Dout <= Dout1;
				else
					Dout <= Dout2;
				end if;
			else
				case A(14 downto 12) is
					when "100" =>
                                          Dout <= IDE_DELAY;
                                        when "000" =>
                                          Dout <= ROM_OVERLAY_ENABLE;
					when others =>
                                          Dout <= FW_VERSION;
				end case;
			end if;
		end if;
	end process data_enable;


	AUTOCONFIG_SM: process(CLK_PLL,RESET)
	begin
		if(RESET = '0' ) then
			--use these presets for CDTV: This makes the DMAC config first!
--			AUTO_CONFIG_PAUSE <='1';
--			AUTO_CONFIG_DONE_CYCLE	<="11";
--			AUTO_CONFIG_DONE	<="11";

			
			--use these presets for normal mode
			AUTO_CONFIG_PAUSE <= '0';
			AUTO_CONFIG_DONE_CYCLE	<= "00";
			AUTO_CONFIG_DONE	<= "00";

			Dout1 <= "1111";
			Dout2 <= "1111";
			SHUT_UP_IDE	<= '1';
			IDE_BASEADR <= x"FF";

			SHUT_UP_MEM <= '1';	
			DSACK_16BIT_AUTOCONFIG <='0';
			
			Z2_MODE <= '0';
			Z2_ADR1 <= "001";
			Z2_ADR1 <= "010";

			
		elsif(rising_edge(CLK_PLL)) then
			
			--Autoconfig(tm) data-encoding	
			if(AUTO_CONFIG = '1' and AS_030= '1' and AS_030_D0= '0')then
				AUTO_CONFIG_DONE <= AUTO_CONFIG_DONE_CYCLE OR AUTO_CONFIG_DONE;
			end if;
		
			--delay one autoconfig-cycle for cdtv!	
			if( (A(31 downto 16) =x"00E8" and A(6 downto 1)= "100100" and AS_030= '1' and AS_030_D0= '0' and AUTO_CONFIG_PAUSE ='1') or
				(OPTION ='1' and AUTO_CONFIG_PAUSE ='1'))then
				AUTO_CONFIG_PAUSE <='0';
				AUTO_CONFIG_DONE	<= "00";
				AUTO_CONFIG_DONE_CYCLE <= "00";
			end if;

			
			if((A(31 downto 16) =x"00E8" and AS_000 = '0' and BGACK_000='0' and BGACK_000_SAMPLED ='0') -- DMA during AC means 68000-CPU present = Zorro 2 Mode!
				 )then 
				Z2_MODE <= '1';
				--AUTO_CONFIG_PAUSE <='0';
				--AUTO_CONFIG_DONE	<= "00";
				--AUTO_CONFIG_DONE_CYCLE <= "00";
			end if;

			--ac-data decode: permanent!
			--mem/ide order swapped for kick 1.3!
			case A(6 downto 1) is
				when "000000"	=> 
					Dout1 <="1"& Z2_MODE &"10";
					Dout2 <="1101";
				when "000001"	=> 
					Dout1 <="0"& Z2_MODE &"11"; --Z3: Max 128(size extension -autosize) Z2: 4MB
					Dout2 <="1001"; --one Card, 64kb = 001
				when "000011"	=> 
					--ProductID low nibble: 9->0110=6
					Dout1 <="0000";
					Dout2 <="1001";
				when "000100"	=> 
					--extension bits
					Dout1 <="01"& Z2_MODE &"1"; --size extension memory device
					Dout2 <= "1111";
				when "000101"	=> 
					--extension bits
					Dout1 <="111"& Z2_MODE; --autosizing in Z3
					Dout2 <= "1111";
				when "001001"	=> 							
					--Ventor ID 1
					Dout1 <="0101";
					Dout2 <="0111";
				when "001010"	=> 
					--Ventor ID 2
					Dout1 <="1110";
					Dout2 <="1101";
				when "001011"	=> 
					--Ventor ID 3 : $0A1C: A1k.org
					--Ventor ID 3 : $082C: BSC
					Dout1 <="0011";
					Dout2 <="0011";
				when "001100"	=> 
					--Serial byte 0 (msb) high nibble
					--Dout1 <="0100";
					Dout1 <= "1111";
					Dout2 <="0100";
				when "001101"	=> 
					--Serial byte 0 (msb) low  nibble
					--Dout1 <="1110";
					Dout1 <= "1111";
					Dout2 <="1110";
				when "001110"	=> 
					--Serial byte 1       high nibble
					--Dout1 <= "1001";
					Dout1 <= "1111";
					Dout2 <= "1001";
				when "001111"	=> 
					--Serial byte 1       low  nibble
					--Dout1 <= "0100";
					Dout1 <= "1111";
					Dout2 <= "0100";
				when "010010"	=> 
					--Serial byte 3 (lsb) high nibble
					--Dout1 <= "0100";
					Dout1 <= "1111";
					Dout2 <= "0100";
				when "010011"	=> 
					--Serial byte 3 (lsb) low  nibble: B16B00B5
					Dout1 <= not(FW_VERSION);
					Dout2 <= "1010";
				when "010111"	=> 
				   --Rom vector low byte low  nibble
					Dout1 <= "1111";
					Dout2 <= "1110";
				when others	=> 	
					Dout1 <= "1111";
					Dout2 <= "1111";				
			end case;	
		
			if(AUTO_CONFIG = '1' and AS_030_D0 = '0') then

				DSACK_16BIT_AUTOCONFIG <= '1';
				--mem/ide order swapped for kick 1.3!
				case A(6 downto 1) is
					when "100100"	=> 
						if(DS_030 = '0' and RW_030='0' and AUTO_CONFIG_DONE(0) = '1')then 
							--address for Z2 mode (irrelevant for Z3!!!)
							case D(31 downto 29) is
								when "001" =>
									Z2_ADR1 <= "001";
									Z2_ADR2 <= "010";
								when "010" =>
									Z2_ADR1 <= "010";
									Z2_ADR2 <= "011";
								when "011" =>
									Z2_ADR1 <= "011";
									Z2_ADR2 <= "100";
								when "100" =>
									Z2_ADR1 <= "100";
									Z2_ADR2 <= "101";
								when others =>
									Z2_ADR1 <= "001";
									Z2_ADR2 <= "010";
							end case;
							--enable board
							SHUT_UP_MEM <= '0';
							AUTO_CONFIG_DONE_CYCLE	<= "11"; --done here
						end if;
						if(DS_030 = '0' and RW_030='0' and AUTO_CONFIG_DONE (0) = '0')then 
							IDE_BASEADR(7 downto 4)	<= D(31 downto 28); --Base adress
							SHUT_UP_IDE <= '0'; --enable board
							AUTO_CONFIG_DONE_CYCLE	<= "01"; --done here
						end if;
					when "100101"	=> 
						if(DS_030 = '0' and RW_030='0' and AUTO_CONFIG_DONE(0) = '0')then 
							IDE_BASEADR(3 downto 0)	<= D(31 downto 28); --Base adress
						end if;
					when "100110"	=> 
						if(DS_030 = '0' and RW_030='0' and AUTO_CONFIG_DONE = "00")then 
							AUTO_CONFIG_DONE_CYCLE	<= "01"; --done here
						end if;
						if(DS_030 = '0' and RW_030='0' and AUTO_CONFIG_DONE = "01")then 
							AUTO_CONFIG_DONE_CYCLE	<= "11"; --done here
						end if;
					when others	=> 							
				end case;	
			else		
				--reset on no AS-Strobe
				DSACK_16BIT_AUTOCONFIG <= '0';			
			end if;
		end if;
	end process;

	IDE_SM: process(CLK_PLL,RESET,ROM_OVERLAY_ENABLE)
	begin
		if(RESET = '0' ) then
			IDE_ENABLE 		<='0';
			IDE_R_S		<= '1';
			IDE_BUF_DIR <= '1';
			IDE_W_S		<= '1';
			ROM_OE_S	<= '1';
			DSACK_16BIT_IDE <='0';
			IDE_DELAY <= "1111";
			IDE_DSACK_D		<= (others => '0');
			IDE_READY <='0';
			CLK_HALF <='1';
                        CHIP_ROM_OVERLAY_ENABLE <= ROM_OVERLAY_ENABLE(0);      
		elsif(rising_edge(CLK_PLL)) then			

                        if (A(23 downto 4) =x"BFE00" and RW_030 = '0') then --detects writes to $BFE001 to reset OVL
				CHIP_ROM_OVERLAY_ENABLE <= '0';
			end if;

		
			if(IDE_SPACE = '1' and AS_030 = '0')then
				CLK_HALF <= not CLK_HALF;
				--rom overlay enable section			
				if(RW_030='0' and A(15)='1' and AUTO_CONFIG='0')then --enable on write on the last word
                                  if (A(14) = '0') then
                                      ROM_OVERLAY_ENABLE <=  D(31 downto 28);
												 else
                                      IDE_DELAY <= D(31 downto 28);
                                  end if;
				end if;

				
				if(RW_030 = '0')then
					--write to rom-overlayconfig
					IDE_ENABLE  <= '1';
					IDE_W_S		<= A(15); --no IDE write when A(15) is set, then rom-overlay status-bits are written!
					IDE_R_S		<= '1';
					ROM_OE_S	<=	'1';	
					IDE_BUF_DIR <='1';
					if((IDE_WAIT = '1' and IDE_WAIT_D ='1' and IDE_READY ='1') or A(15)='1')then
						DSACK_16BIT_IDE		<=	'1';
					end if;					
				elsif(RW_030 = '1' and IDE_ENABLE = '1' )then
					--read from IDE instead from ROM
					IDE_W_S		<= '1';
					IDE_R_S		<= A(15); --no IDE read when A(15) is set, then rom-overlay or ide waitstates status-bits are read!
					IDE_BUF_DIR <= A(15); -- point IDE-BUFFER away from databus on reads from A(15)='1'
					ROM_OE_S		<=	'1';
					if((IDE_WAIT = '1' and IDE_WAIT_D ='1' and IDE_READY ='1') or A(15)='1')then
						DSACK_16BIT_IDE		<=	'1';
					end if;
				elsif(RW_030 = '1' and IDE_ENABLE = '0')then
					DSACK_16BIT_IDE		<= IDE_DSACK_D(3);
					IDE_W_S		<= '1';
					IDE_R_S		<= '1';
					ROM_OE_S	<=	'0';
					IDE_BUF_DIR <=	'0'; --read from ROM
				end if;	
				
				if(IDE_W_S = '0' or IDE_R_S ='0')then
					IDE_WAIT_D <= IDE_WAIT;				
				else
					IDE_WAIT_D <= '0';
				end if;
				
				if(CLK_HALF = '0')then	
					IDE_DSACK_D <=	IDE_DSACK_D +1;				
				end if;
				if(IDE_DSACK_D= IDE_DELAY )then
					IDE_READY <='1';
				end if;
			else
				IDE_WAIT_D <= '0';
				CLK_HALF <='1';
				IDE_READY <='0';
				DSACK_16BIT_IDE <='0';
				IDE_BUF_DIR <= '1';
				IDE_R_S		<= '1';
				IDE_W_S		<= '1';
				ROM_OE_S	<= '1';
				IDE_DSACK_D		<= (others => '0');		
			end if;						
		end if;		
	end process;

	-- bus drivers
	AMIGA_BUS_ENABLE_HIGH <= '0' WHEN BGACK_030_INT ='1' and AS_030_000_SYNC='0' and AS_030 = '0' else --not (SM_AMIGA = IDLE_P or (SM_AMIGA = END_CYCLE_N and CLK_000 = '1')) ELSE 
							 '0' WHEN (BGACK_030_INT='0' ) AND AMIGA_BUS_ENABLE_DMA_HIGH = '0' ELSE
							 '1';
	AMIGA_BUS_ENABLE_LOW <=  '0' WHEN (BGACK_030_INT='0' ) AND AMIGA_BUS_ENABLE_DMA_LOW = '0'   ELSE
							 '1';  
	
	
	AMIGA_BUS_DATA_DIR 	 <= RW_030 WHEN (BGACK_030_INT ='1') ELSE --Amiga READ/WRITE
							'0' WHEN (RW_000='1' AND BGACK_030_INT='0' AND TK_CYCLE = '0' AND AS_000 = '0') ELSE --DMA READ from TK-expansion space
							'1'; --Point towarts TK
	--ide stuff

	IDE_R		<= IDE_R_S when AS_030 ='0' else '1';
	IDE_W		<= IDE_W_S when AS_030 ='0' else '1';
	ROM_EN	<= ROM_OE_S when AS_030 ='0' else '1';
	D(31 downto 28)	<=	Dout when Dout_ENABLE = '1' else "ZZZZ";
 	
	--dma stuff
	DTACK	<= 	'Z';--DTACK will be generated by GARY!
	
	AS_030	<= 	'Z' when BGACK_030_INT ='1' else
			   	'0' when AS_000_DMA ='0' and AS_000 ='0' else 
			   	'1';
	DS_030	<= 	'Z' when BGACK_030_INT ='1' else
				'0' when DS_000_DMA ='0' and AS_000 ='0' else 
			   	'1';
	A(0)		<= 	A0_DMA when (BGACK_030_INT='0' ) --drive on DMA-Cycle
							else	'Z'; --tristate on CPU-Cycle
	A(31 downto 24)		<= 	"00000000" when (BGACK_030_INT='0' ) --drive on DMA-Cycle
							else	(others => 'Z'); --tristate on CPU-Cycle
	A(23 downto 1) <= (others => 'Z');
	SIZE	<= 	SIZE_DMA when (BGACK_030_INT='0' )
						else "ZZ"; --tristate on CPU-Cycle
	--rw
	RW_030		<= 	RW_000_DMA when (BGACK_030_INT='0' )  --drive on DMA-Cycle
						else	'Z'; --tristate on CPU-Cycle
	
	BR_030	<= BR_000;	
	BGACK_030 <= BGACK_030_INT;

	--e and VMA		
	E		<= ESig;
--	E		<= '1' when 
--							cpu_est = E7 or
--							cpu_est = E8 or
--							cpu_est = E9 or
--							cpu_est = E10 						
--						else '0';
	VMA		<= 'Z' when BGACK_030_INT ='0' or RESET = '0' else VMA_INT;
	
		
	--as and uds/lds
	AS_000	<=  'Z' when (BGACK_030_INT='0' or RESET = '0'  ) else
							'0' when AS_000_INT ='0' and AS_030 ='0' else 
			   			'1';
	RW_000	<=  'Z' when (BGACK_030_INT='0' or RESET = '0' )  --tristate on DMA-cycle
							else RW_000_INT; -- drive on CPU cycle

	UDS_000	<=  'Z' when (BGACK_030_INT='0' or RESET = '0'  ) else --tristate on DMA cycle
							UDS_000_INT when DS_000_ENABLE ='1' -- output on cpu cycle
							else '1'; -- datastrobe not ready jet
	LDS_000	<= 	'Z' when (BGACK_030_INT='0' or RESET = '0'  ) else --tristate on DMA cycle
							LDS_000_INT when  DS_000_ENABLE ='1' -- output on cpu cycle
							else '1'; -- datastrobe not ready jet



	
	--sdram stuff
	--SD-RAM clock-stuff
	CLK_RAM <= not CLK_PLL;
	CLK_EN 	<= ENACLK_PRE;

	
	--dsack
	DSACK	<= 			"11" when AS_030 ='1' else 
							"01" when DSACK_16BIT_AMIGA = '1' or DSACK_16BIT_IDE ='1' or DSACK_16BIT_AUTOCONFIG ='1' else -- output on amiga/ac/ide cycle
							"11";
	STERM		<= STERM_S2 when AS_030 ='0' else '1';

	--fpu
	FPU_CS		<=	'0' when FC ="111" and A(19 downto 16)= "0010" AND BGACK_000='1' else '1';--FPU_CYCLE;
	
	--if no copro is installed:
	BERR		<=	'0'  when FPU_CYCLE='0' and FPU_SENSE ='1' and AS_030 ='0' else 'Z';


	--cache inhibit:  Tristate for expansion (it decides) and off for the Amiga 
	CIIN <= '1' WHEN A(31 downto 20) = x"00F" and AS_030 ='0' ELSE -- Enable for Kick-rom
			'1' WHEN MEM_SPACE = '1' and AS_030 ='0' ELSE 
			'0'; --off for the Amiga
	CBACK <= CBACK_S when MEM_SPACE = '1' and AS_030 ='0' else '1';	
	
	--AVEC
	AVEC 	<=	'0' when AVEC_CYCLE = '0' and AS_030 ='0' else '1';
				 
end Behavioral;

