# 68030-TK2
 
This is the second version of my 68030 CPU turbo-card for the 68000-CPU Slot of an Amiga 500, 1000 and 2000. It doesn't fit into a CDTV without an adapter.
![Prototype rev 0.1](https://gitlab.com/MHeinrichs/68030-TK2/raw/master/Pics/V2a-Working.jpg)

The CPU frequency is very adjustable and must be PGA-package. Possible and tested frequencies are: 14, 15, 17, 20, 25, 28, 30, 35, 40, 50, 56 and 60 MHz. You should find an apropirate frequency for your CPU ;) .
The RAM-expansion has up to 128MB of SD-RAM on board (autosizing). Furthermore, it has a BSC-AT-Bus-compatible IDE controller on board and works with the Oktapussy driver found on the [aminet](http://aminet.net/package/disk/misc/oktapus "Online for ages!" ) (sorry for the name but that's what it's called).
Finally a FPU (PLCC68 package) can be inserted running at half, full or even double speed of the CPU (doubble speed is not available for CPU-frequencies >30 MHz). 

The IDE-controller supports Kickstart 1.3 and runs in a "save-mode" with a maximum of 3.5MB/s. You can unlock the full speed using the [maprom tool](https://gitlab.com/scratje/maprom020).

**Known bug/limitation:** 
Kick1.3 does not support Zorro3-autoconfig(tm). Since the memory is a Z3-Memory it must be added manually starting at address $40000000 eg using [AutoAddRAM](http://aminet.net/package/util/boot/AutoAddRAM). Furthermore the AC-chain stops under Kick1.3 and no further expansions are detected. Kickstart >=2.0 does not have this limitation.

**Importaint hints for building that thing!**
There are four importaint things you have to consider, when you are building that thing:
1. You have to cut the pins of the FPU-socket (IC17) at the bottom of the PCB. Otherwise the pins will press into the kickstart-rom and this adds 1-2mm of height and the keyboard does not fit anymore! Do this by putting the socket into the PCB, solder one pin to fix the socket and then carefully clip all remaining pins just above the bottom side of the PCB. Solder the remaining pins. Finally, desolter, clip and resolder the first pin. If you clip all pins from the socket before youinsert it into the PCB, you will have a dificuld task ahead (I know what I'm talking about ;) )
2. The oscillator X1 must be soldered before the ICS570-chip, because they are so close to each other.
3. Check your soldering of the ICs near the IDE-connector thoroughly. They are blocked by the IDE-connector latter on.
4. If you use a 90°-connector for the IDE-port, solder the port as high as possible into the pcb, because otherwise there is a nasty capacitor on the A500-mainboard blocking your IDE-port.

**DISCLAIMER!!!!**  
**THIS IS A HOBBYIST PROJECT! I'M NOT RESPONSIBLE IF IT DOESN'T WORK OR DESTROYS YOUR VALUABLE DATA!**

## License
1. Do whatever you want with this, if you do it for private use and mention this work as origin. 
2. If you want to make money out of this work, ask me first!
3. If you copy parts of this work to your project, mention the source.
4. If you use parts of this work for closed source, burn in hell and live in fear!

## Theory of operation
* This PCB has four layers and consists of the CPU, a buffered IDE-Bus, an AutoBoot-ROM, a controlling Xilinx XC95288XL CPLD, two 64MB SD-RAM Chips running up to 120MHz a PLL and various BUS-drivers and latches.
* The CPLD does various things:
    * It runs the bus translation betweed the 68000-host and the 68030 CPU
    * It generates the E-Clock and controlls the 6800-preipheral cycles
    * It does the DMA controll
    * It controlls the SD-RAM 
    * It autoconfigures the RAM for the Amiga in the Z3-address space 
    * It autoconfigures the IDE

## What is in this repository?
This repository consists of several directories:
* Layout: The board, schematic and bill of material (BOM) are in this directory.
* Logic: Here the code, project files and pin-assignments for the CPLD is stored
* Pics: Some pictures of this project
* root directory: the readme

## How to open the PCB-files?
You find the eagle board and schematic files (brd/sch) for the pcb. The software can be [downloaded](https://www.autodesk.com/products/eagle/overview) for free after registration. KiCad can import these files too. But importers are not 100% reliable... 

## How to make a PCB
You can generate Gerber-files from the brd files according to the specs of your PCB-manufactor. However, you can try to use the Gerber-files provided. Some specs: min trace width: 0.15mm/6mil, min trace dist: 0.15mm/6mil, min drill: 0.3mm

**ATTENTION: THE PCB has 4 layers!**

## How to get the parts?
Most parts can be found on digikey or mouser or a parts supplier of your choice. The CPU is the most difficult part to get. Be aware of ebay! A lot of fake CPUs are offered! A list of parts is found in the file [68030TK2-V02a.txt](https://gitlab.com/MHeinrichs/68030-tk2/blob/master/Layout/68030TK2-V02a.txt)

## How to programm the board?
The CPLD must be programmed via Xilinx IMpact and an apropriate JTAG-dongle. The JED-File is provided. Additionally you have to programm an 29F010 or 29F040 and put it in the socket for autoboot. Otherwise the autoboot-function will not be enabled and you have to use the programm "AT" in the [oktapussy-driver package](http://aminet.net/package/disk/misc/oktapus) to mount your IDE-drives.

## It doesn't work! How to I get help?
For support visit the [a1k-forum](https://www.a1k.org/forum). Yes, you need to register and it's German, but writing in English is accepted. The general support thread in the forum is [here](https://www.a1k.org/forum/index.php?threads/78469/). For bug-reports write a ticket here ;). 

